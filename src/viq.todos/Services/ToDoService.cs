﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using ProtoBuf.Grpc;
using System.Threading.Tasks;
using VIQ.ToDos.Contract.Models;
using VIQ.ToDos.Contract.Services;
using CreateToDo = VIQ.ToDos.Features.CreateToDo;
using ListToDo = VIQ.ToDos.Features.ListToDo;
using RemoveToDo = VIQ.ToDos.Features.DeleteToDo;
using UpdateToDo = VIQ.ToDos.Features.UpdateToDo;

namespace VIQ.ToDos.Services
{
    [AllowAnonymous]
    public class ToDoService : IToDoService
    {
        private readonly IMediator _mediator;

        public ToDoService(IMediator mediator)
        {
            _mediator = mediator;
        }

        public Task<CreateToDoResponse> CreateToDoAsync(CreateToDoRequest request, CallContext context = default) =>
            _mediator.Send(new CreateToDo.Command(request), context.CancellationToken);

        public Task<ListToDoResponse> ListToDoAsync(ListToDosRequest request, CallContext context = default) =>
            _mediator.Send(new ListToDo.Query(request), context.CancellationToken);

        public Task<DeleteToDoResponse> RemoveToDoAsync(DeleteToDoRequest request, CallContext context = default) =>
            _mediator.Send(new RemoveToDo.Command(request), context.CancellationToken);

        public Task<UpdateToDoResponse> UpdateToDoAsync(UpdateToDoRequest request, CallContext context = default) =>
            _mediator.Send(new UpdateToDo.Command(request), context.CancellationToken);
    }
}
