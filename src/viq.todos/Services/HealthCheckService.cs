﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using ProtoBuf.Grpc;
using System.Threading.Tasks;
using VIQ.Service.Base.Models.Request;
using VIQ.Service.Base.Models.Responses;
using VIQ.Service.Base.Services.HealthCheck;
using HealthCheckFeature = VIQ.ToDos.Features.HealthChecks.HealthCheckFeature;
using PingFeature = VIQ.ToDos.Features.HealthChecks.PingFeature;

namespace VIQ.ToDos.Services
{
    [AllowAnonymous]
    public class HealthCheckService : IHealthCheckService
    {
        private static readonly IRequest<HealthCheckResponse> _healthCheckRequest = new HealthCheckFeature.Query();
        private static readonly IRequest<PingResponse> _pingRequest = new PingFeature.Query();

        private readonly IMediator _mediator;

        public HealthCheckService(IMediator mediator)
        {
            _mediator = mediator;
        }

        public Task<HealthCheckResponse> HealthCheckAsync(HealthCheckRequest request, CallContext context = default) =>
            _mediator.Send(_healthCheckRequest);

        public Task<PingResponse> PingAsync(PingRequest request, CallContext context = default) =>
            _mediator.Send(_pingRequest);
    }
}
