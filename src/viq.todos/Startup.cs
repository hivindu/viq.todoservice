﻿using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using ProtoBuf.Grpc.Server;
using ServiceRegister;
using System.Reflection;
using VIQ.Service.Base.Extensions;
using VIQ.Service.Base.Services.Discovery;
using VIQ.Service.Base.Services.Discovery.Authentication;
using VIQ.Service.Datastore.Cosmosdb.Extensions;
using VIQ.Service.Datastore.Cosmosdb.Models;
using VIQ.ToDos.Contract;
using VIQ.ToDos.Services;

namespace VIQ.ToDos
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IWebHostEnvironment hostEnvironment)
        {
            Configuration = configuration;
            HostEnvironment = hostEnvironment;
        }

        public IConfiguration Configuration { get; }

        public IWebHostEnvironment HostEnvironment { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            ConfigureOptions(services);
            ConfigureApplicationServices(services);
            ConfigureAuthentication(services);
            ConfigureGrpc(services);
            ConfigureMvc(services);
            ConfigureMediatR(services);
        }

        public void Configure(IApplicationBuilder app, IServiceProvider<ToDoContract> serviceProvider)
        {
            if (HostEnvironment.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapGrpcService<HealthCheckService>();
                endpoints.MapGrpcService<ToDoService>();

                if (HostEnvironment.IsDevelopment())
                {
                    endpoints.MapCodeFirstGrpcReflectionService();
                }

                endpoints.MapControllers();
                endpoints.MapGet("/", context =>
                {
                    return context.Response.WriteAsync("Hello from VIQ.Teams");
                });
            });

            if (HostEnvironment.IsDevelopment())
            {
                serviceProvider.Register(unregisterOnExit: true, ignoreErrors: true);
            }
            else
            {
                serviceProvider.Register(unregisterOnExit: false, ignoreErrors: false);
            }
        }

        private static void ConfigureApplicationServices(IServiceCollection services)
        {
            services.AddHttpContextAccessor(); // to access httpcontext inside a service
            services.ConfigureApplicationServices(options => options.ScanForModuleAssemblies = true); // this finds classes wich has Singelton
        }

        private static void ConfigureMvc(IServiceCollection services)
        {
            services.AddControllers();
            services.AddApiControllers<ToDoContract>();
        }

        private static void ConfigureMediatR(IServiceCollection services)
        {
            var modules = new Assembly[]
            {
                typeof(Startup).Assembly,
            };

            services.AddMediatR(modules);
        }

        private void ConfigureAuthentication(IServiceCollection services)
        {
            var serviceContract = Configuration.GetSection("ServiceContract").Get<ToDoContract>();
            services.ConfigureAuthentication(serviceContract);
        }

        private void ConfigureOptions(IServiceCollection services)
        {
            services.AddDatabasePropertiesConfigurator();
            services.Configure<CosmosConfiguration>(Configuration.GetSection("Database"));
            services.Configure<ToDoContract>(Configuration.GetSection("ServiceContract"));
        }

        private void ConfigureGrpc(IServiceCollection services)
        {
            services.AddCodeFirstGrpc(options =>
            {
                options.IgnoreUnknownServices = true;
                options.EnableDetailedErrors = HostEnvironment.IsDevelopment();
            });

            if (HostEnvironment.IsDevelopment())
            {
                services.AddCodeFirstGrpcReflection(); // create an endpoint to get meta data to render that UI.
            }
        }
    }
}
