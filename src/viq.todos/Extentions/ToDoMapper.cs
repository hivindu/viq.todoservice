﻿using System.Collections.Generic;
using VIQ.ToDos.Contract.Models.Data;
using VIQ.ToDos.Models;

namespace VIQ.ToDos.Extentions
{
    public static class ToDoMapper
    {
        public static ToDoData ToToDoDataModel(this ToDo toDoData) => new ()
        {
            ID = toDoData.Id,
            Work = toDoData.Work,
            Status = toDoData.Status,
        };

        public static ToDoData[] ToToDoList(this ToDo[] toDoData)
        {
            ToDoData[] toDoDataSet = new ToDoData[toDoData.Length];

            if (toDoData.Length > 0)
            {
                for (int i = 0; i < toDoData.Length; i++)
                {
                    var todo = toDoData[i];
                    toDoDataSet[i] = todo.ToToDoDataModel();
                }
            }

            return toDoDataSet;
        }
    }
}
