﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using VIQ.Service.Base.Services.Server;

namespace VIQ.ToDos
{
    public class Program
    {
        public static void Main(string[] args)
        {
            ServerUtils.PrintLogo("TODO API");
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}
