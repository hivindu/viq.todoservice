﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;
using VIQ.Service.Base.Constants;
using VIQ.Service.Base.Models;
using VIQ.Service.Datastore.Cosmosdb.Services;
using VIQ.ToDos.Contract.Models;
using VIQ.ToDos.Extentions;
using VIQ.ToDos.Models;

namespace VIQ.ToDos.Features.CreateToDo
{
    public class Handler : IRequestHandler<Command, CreateToDoResponse>
    {
        private readonly IDbOperations<ToDo> _todoDbOperations;

        public Handler(IDbOperations<ToDo> todoDbOperations)
        {
            _todoDbOperations = todoDbOperations;
        }

        public async Task<CreateToDoResponse> Handle(Command request, CancellationToken cancellationToken = default)
        {
            if (string.IsNullOrEmpty(request.Data.Work))
            {
                return new CreateToDoResponse(new GrpcError
                {
                    Title = "ToDo null exception",
                    Details = "TODO is Required",
                    ErrorCode = ErrorCodes.InvalidInput,
                });
            }

            var newToDo = new ToDo()
            {
                Work = request.Data.Work,
            };

            newToDo = await _todoDbOperations.CreateAsync(newToDo, cancellationToken).ConfigureAwait(false);

            return new CreateToDoResponse(newToDo.ToToDoDataModel());
        }
    }
}
