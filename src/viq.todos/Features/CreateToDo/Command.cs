﻿using VIQ.Service.Base.Models.Request;
using VIQ.ToDos.Contract.Models;

namespace VIQ.ToDos.Features.CreateToDo
{
    public class Command : GrpcRequest<CreateToDoRequest, CreateToDoResponse>
    {
        public Command(CreateToDoRequest request)
            : base(request)
        {
        }
    }
}
