﻿using VIQ.Service.Base.Models.Request;
using VIQ.ToDos.Contract.Models;

namespace VIQ.ToDos.Features.DeleteToDo
{
    public class Command : GrpcRequest<DeleteToDoRequest, DeleteToDoResponse>
    {
        public Command(DeleteToDoRequest request)
            : base(request)
        {
        }
    }
}
