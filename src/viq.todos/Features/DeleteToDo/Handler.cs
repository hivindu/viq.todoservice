﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;
using VIQ.Service.Base.Constants;
using VIQ.Service.Base.Models;
using VIQ.Service.Datastore.Cosmosdb.Services;
using VIQ.ToDos.Contract.Models;
using VIQ.ToDos.Contract.Models.Data;
using VIQ.ToDos.Models;

namespace VIQ.ToDos.Features.DeleteToDo
{
    public class Handler : IRequestHandler<Command, DeleteToDoResponse>
    {
        private readonly IDbOperations<ToDo> _todoDbOperations;

        public Handler(IDbOperations<ToDo> todoDbOperations)
        {
            _todoDbOperations = todoDbOperations;
        }

        public async Task<DeleteToDoResponse> Handle(Command request, CancellationToken cancellationToken = default)
        {
            if (string.IsNullOrEmpty(request.Data.ID))
            {
                return new DeleteToDoResponse(new GrpcError
                {
                    Title = "ToDo null exception",
                    Details = "ToDo Id is null",
                    ErrorCode = ErrorCodes.InvalidInput,
                });
            }

            var newToDo = new ToDo()
            {
                Id = request.Data.ID,
            };

            bool isDeleted = await _todoDbOperations.DeleteAsync(newToDo, cancellationToken).ConfigureAwait(false);

            var isDeletedModel = new ToDoDeleteBasic()
            {
                IsDeleted = isDeleted,
            };

            return new DeleteToDoResponse(isDeletedModel);
        }
    }
}
