﻿using VIQ.Service.Base.Models.Request;
using VIQ.ToDos.Contract.Models;

namespace VIQ.ToDos.Features.UpdateToDo
{
    public class Command : GrpcRequest<UpdateToDoRequest, UpdateToDoResponse>
    {
        public Command(UpdateToDoRequest request)
            : base(request)
        {
        }
    }
}
