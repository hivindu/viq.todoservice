﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;
using VIQ.Service.Base.Constants;
using VIQ.Service.Base.Models;
using VIQ.Service.Datastore.Cosmosdb.Services;
using VIQ.ToDos.Contract.Models;
using VIQ.ToDos.Extentions;
using VIQ.ToDos.Models;

namespace VIQ.ToDos.Features.UpdateToDo
{
    public class Handler : IRequestHandler<Command, UpdateToDoResponse>
    {
        private readonly IDbOperations<ToDo> _toDoDbOperations;

        public Handler(IDbOperations<ToDo> toDoDbOperations)
        {
            _toDoDbOperations = toDoDbOperations;
        }

        public async Task<UpdateToDoResponse> Handle(Command request, CancellationToken cancellationToken = default)
        {
            if (string.IsNullOrEmpty(request.Data.Id))
            {
                return new UpdateToDoResponse(new GrpcError
                {
                    Title = "ToDo null exception",
                    Details = "Id feild is required",
                    ErrorCode = ErrorCodes.InvalidInput,
                });
            }

            if (string.IsNullOrEmpty(request.Data.Work))
            {
                return new UpdateToDoResponse(new GrpcError
                {
                    Title = "ToDo null exception",
                    Details = "Work feild is required",
                    ErrorCode = ErrorCodes.InvalidInput,
                });
            }

            var result = await _toDoDbOperations.GetAsync(request.Data.Id, cancellationToken).ConfigureAwait(false);

            if (result is null)
            {
                return new UpdateToDoResponse(new GrpcError
                {
                    Title = "Argument null exception",
                    Details = "ToDo is not found",
                    ErrorCode = ErrorCodes.InvalidInput,
                });
            }

            result.Id = request.Data.Id;
            result.Work = request.Data.Work;
            result.Status = request.Data.Status;

            result = await _toDoDbOperations.UpdateAsync(result, cancellationToken).ConfigureAwait(false);

            return new UpdateToDoResponse(result.ToToDoDataModel());
        }
    }
}
