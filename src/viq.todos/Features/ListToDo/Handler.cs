﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;
using VIQ.Service.Base.Models;
using VIQ.Service.Datastore.Cosmosdb.Services;
using VIQ.ToDos.Contract.Models;
using VIQ.ToDos.Contract.Models.Data;
using VIQ.ToDos.Extentions;
using VIQ.ToDos.Models;

namespace VIQ.ToDos.Features.ListToDo
{
    public class Handler : IRequestHandler<Query, ListToDoResponse>
    {
        private readonly IDbOperations<ToDo> _todoDbOperations;

        public Handler(IDbOperations<ToDo> todoDbOperations)
        {
            _todoDbOperations = todoDbOperations;
        }

        public async Task<ListToDoResponse> Handle(Query request, CancellationToken cancellationToken = default)
        {
            var orderByQuery = string.Empty;

            if (!string.IsNullOrEmpty(request.Data.PaginationData.OrderBy))
            {
                var (orderBy, orderDirection) = _todoDbOperations.SanitizeOrderBy(
                    request.Data.PaginationData.OrderBy,
                    request.Data.PaginationData.OrderDirection ?? "ASC");

                orderByQuery = $"ORDER BY T.{orderBy} {orderDirection}";
            }

            var typeQueryParam = new QueryParameter("@type", typeof(ToDo).FullName!);

            var toDoCount = _todoDbOperations.CountAsync(
                $"SELECT VALUE(COUNT(T)) FROM T WHERE T.Type = @type",
                cancellationToken,
                typeQueryParam);

            var toDos = _todoDbOperations.QueryAsync(
                $"SELECT * FROM T WHERE T.Type = @type {orderByQuery}",
                cancellationToken,
                typeQueryParam);

            await Task.WhenAll(toDoCount, toDos).ConfigureAwait(false);

            var paginationData = new PaginationData()
            {
                Count = toDoCount.Result,
                Limit = request.Data.PaginationData.Limit,
                Offset = request.Data.PaginationData.Offset,
            };

            var toDoResult = toDos.Result;

            var newToDoResults = ToDoMapper.ToToDoList(toDoResult);

            return new ListToDoResponse(newToDoResults, paginationData);
        }
    }
}
