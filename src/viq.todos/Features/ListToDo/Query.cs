﻿using VIQ.Service.Base.Models.Request;
using VIQ.ToDos.Contract.Models;

namespace VIQ.ToDos.Features.ListToDo
{
    public class Query : GrpcRequest<ListToDosRequest, ListToDoResponse>
    {
        public Query(ListToDosRequest request)
            : base(request)
        {
        }
    }
}
