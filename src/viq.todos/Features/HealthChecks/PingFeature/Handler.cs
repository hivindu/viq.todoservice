﻿using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;
using VIQ.Service.Base.Models.Responses;
using VIQ.Service.Base.Services.Server;

namespace VIQ.ToDos.Features.HealthChecks.PingFeature
{
    public class Handler : IRequestHandler<Query, PingResponse>
    {
        private static readonly string ApiVersion = ServerUtils.GetApiVersion();

        public Task<PingResponse> Handle(Query request, CancellationToken cancellationToken = default)
        {
            return Task.FromResult(new PingResponse
            {
                DateTime = DateTime.UtcNow,
                Version = ApiVersion,
            });
        }
    }
}
