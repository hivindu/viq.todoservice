﻿using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;
using VIQ.Service.Base.Models;
using VIQ.Service.Base.Models.Responses;
using VIQ.Service.Datastore.Cosmosdb.Services;
using VIQ.ToDos.Models;

namespace VIQ.ToDos.Features.HealthChecks.HealthCheckFeature
{
    public class Handler : IRequestHandler<Query, HealthCheckResponse>
    {
        private readonly IDbOperations<ToDo> _toDoDbOperations;

        public Handler(IDbOperations<ToDo> toDoDbOperations)
        {
            _toDoDbOperations = toDoDbOperations;
        }

        public async Task<HealthCheckResponse> Handle(Query request, CancellationToken cancellationToken = default)
        {
            var response = new HealthCheckResponse();

            await Task.WhenAll(CheckDbOperationsAsync(response)).ConfigureAwait(false);

            return response;
        }

        public async Task CheckDbOperationsAsync(HealthCheckResponse response)
        {
            try
            {
                await _toDoDbOperations.QuerySingleAsync(
                    "SELECT * FROM T WHERE Type = @type",
                    CancellationToken.None,
                    new QueryParameter("@type", typeof(ToDo).FullName!)).ConfigureAwait(false);

                response.AddResult(nameof(ToDos), HealthCheckResult.HealthlyResult());
            }
            catch (Exception ex)
            {
                response.AddResult(nameof(ToDos), HealthCheckResult.UnhealthyResult(ex.Message));
            }
        }
    }
}
