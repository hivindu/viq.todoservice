﻿using MediatR;
using Microsoft.Extensions.Logging;
using VIQ.Service.Base.Models.Responses;
using Base = VIQ.Service.Base.Controllers;
using HealthCheckFeature = VIQ.ToDos.Features.HealthChecks.HealthCheckFeature;
using PingFeature = VIQ.ToDos.Features.HealthChecks.PingFeature;

namespace VIQ.ToDos.Controllers
{
    public class HealthCheckController : Base.HealthCheckController
    {
        private static readonly IRequest<HealthCheckResponse> _healthCheckRequest = new HealthCheckFeature.Query();
        private static readonly IRequest<PingResponse> _pingRequest = new PingFeature.Query();

        public HealthCheckController(IMediator mediator, ILogger<HealthCheckController> logger)
            : base(logger, mediator, _pingRequest, _healthCheckRequest)
        {
        }
    }
}
