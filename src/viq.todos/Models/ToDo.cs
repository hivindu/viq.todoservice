﻿using System;
using viq.documents.Attributes;
using viq.services.core.Documents;

namespace VIQ.ToDos.Models
{
    public class ToDo : IDocument
    {
        public string Id { get; set; } = $"{Guid.NewGuid()}";

        public string Work { get; set; } = string.Empty;

        public bool Status { get; set; } = false;

        [AttachSharedAccessToken]
#pragma warning disable CS8766 // Nullability of reference types in return type doesn't match implicitly implemented member (possibly because of nullability attributes).
        public string? ETag { get; set; }
#pragma warning restore CS8766 // Nullability of reference types in return type doesn't match implicitly implemented member (possibly because of nullability attributes).

        public string Type => typeof(ToDo).FullName!;

        public DateTime CreatedDate { get; set; }

        public DateTime? LastModifiedDate { get; set; }

        public string PartitionKey => Type;

        public bool IsDeleted { get; set; }

        public bool ShouldSerializeETag() => false;
    }
}
