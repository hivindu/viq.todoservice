﻿using VIQ.Service.Base.Models.Discovery;

namespace VIQ.ToDos.Contract
{
    public class ToDoContract : IServiceContract
    {
        public string ServiceName => "VIQ.ToDos";

        public string ServiceVersion { get; } = $"{typeof(ToDoContract).Assembly.GetName().Version}";

        public string SecurityKey { get; set; } = string.Empty;

        public string ServiceUrl { get; set; } = "https://localhost:5053";

        public string GatewayUrl { get; set; } = "https://localhost:5000";
    }
}
