﻿using ProtoBuf.Grpc;
using System.ServiceModel;
using System.Threading.Tasks;
using VIQ.ToDos.Contract.Models;

namespace VIQ.ToDos.Contract.Services
{
    [ServiceContract]
    public interface IToDoService
    {
        [OperationContract]
        public Task<ListToDoResponse> ListToDoAsync(ListToDosRequest request, CallContext context = default);

        [OperationContract]
        public Task<CreateToDoResponse> CreateToDoAsync(CreateToDoRequest request, CallContext context = default);

        [OperationContract]
        public Task<UpdateToDoResponse> UpdateToDoAsync(UpdateToDoRequest request, CallContext context = default);

        [OperationContract]
        public Task<DeleteToDoResponse> RemoveToDoAsync(DeleteToDoRequest request, CallContext context = default);
    }
}
