﻿using Microsoft.Extensions.Configuration;
using ServiceRegister;
using System;
using System.Threading;
using System.Threading.Tasks;
using VIQ.Service.Base.Services.Discovery;

namespace VIQ.ToDos.Contract.Services
{
    [SingletonService]
    public class DiscoverableToDoService : DiscoverableService<IToDoService>, IDiscoverableService<IToDoService>
    {
        private readonly ToDoContract _contract = new ();

        public DiscoverableToDoService(IServiceProvider services, IConfiguration configuration)
            : base(services, configuration)
        {
        }

        public override string ServiceName => _contract.ServiceName;

        public override Task<IToDoService?> GetServiceAsync(CancellationToken cancellationToken = default) => GetServiceAsync(ServiceName, cancellationToken);
    }
}
