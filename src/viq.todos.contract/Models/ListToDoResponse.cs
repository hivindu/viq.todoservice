﻿using System;
using System.Runtime.Serialization;
using System.Text.Json.Serialization;
using VIQ.Service.Base.Models;
using VIQ.Service.Base.Models.Responses;
using VIQ.ToDos.Contract.Models.Data;

namespace VIQ.ToDos.Contract.Models
{
    [DataContract]
    public class ListToDoResponse : GrpcPagedListResponse<ToDoData>
    {
        public ListToDoResponse(ToDoData[] data, PaginationData pagination)
        {
            Data = data;
            Pagination = pagination;
        }

        public ListToDoResponse(GrpcError error)
        {
            Error = error;

            Data = Array.Empty<ToDoData>();
            Pagination = new PaginationData();
        }

        public ListToDoResponse()
        {
            Data = Array.Empty<ToDoData>();
            Pagination = new PaginationData();
        }

        [JsonIgnore]
        [DataMember(Order = 1)]
        public override PaginationData Pagination { get; set; }

        [DataMember(Order = 2)]
        public override ToDoData[] Data { get; set; }

        [DataMember(Order = 3)]
        public override GrpcError? Error { get; set; }
    }
}
