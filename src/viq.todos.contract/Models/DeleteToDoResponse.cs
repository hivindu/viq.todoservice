﻿using System.Runtime.Serialization;
using VIQ.Service.Base.Models;
using VIQ.Service.Base.Models.Responses;
using VIQ.ToDos.Contract.Models.Data;

namespace VIQ.ToDos.Contract.Models
{
#nullable enable
    [DataContract]
    public class DeleteToDoResponse : GrpcResponse
    {
        public DeleteToDoResponse(ToDoDeleteBasic isDeleted)
        {
            IsDeleted = isDeleted;
        }

        public DeleteToDoResponse(GrpcError error)
        {
            Error = error;
        }

        public DeleteToDoResponse()
        {
        }

        [DataMember(Order = 1)]
        public override GrpcError? Error { get; set; }

        [DataMember(Order = 2)]
        public ToDoDeleteBasic? IsDeleted { get; set; }
    }
}
