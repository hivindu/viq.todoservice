﻿using System.Runtime.Serialization;

namespace VIQ.ToDos.Contract.Models.Data
{
    [DataContract]
    public class ToDoDeleteBasic
    {
        [DataMember(Order = 1)]
        public bool IsDeleted { get; set; }
    }
}
