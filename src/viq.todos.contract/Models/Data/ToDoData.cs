﻿using System.Runtime.Serialization;

namespace VIQ.ToDos.Contract.Models.Data
{
    [DataContract]
    public class ToDoData
    {
        [DataMember(Order = 1)]
        public string ID { get; set; } = string.Empty;

        [DataMember(Order = 2)]
        public string Work { get; set; } = string.Empty;

        [DataMember(Order = 3)]
        public bool Status { get; set; } = false;
    }
}
