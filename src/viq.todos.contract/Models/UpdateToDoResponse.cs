﻿using System.Runtime.Serialization;
using VIQ.Service.Base.Models;
using VIQ.Service.Base.Models.Responses;
using VIQ.ToDos.Contract.Models.Data;

namespace VIQ.ToDos.Contract.Models
{
    [DataContract]
    public class UpdateToDoResponse : GrpcResponse
    {
        public UpdateToDoResponse()
        {
        }

        public UpdateToDoResponse(GrpcError error)
        {
            Error = error;
        }

        public UpdateToDoResponse(ToDoData response)
        {
            ToDoResponse = response;
        }

#nullable enable
        [DataMember(Order = 1)]
        public override GrpcError? Error { get; set; }

        public ToDoData? ToDoResponse { get; set; }
    }
}
