﻿using System.Runtime.Serialization;

namespace VIQ.ToDos.Contract.Models
{
    [DataContract]
    public class CreateToDoRequest
    {
        [DataMember(Order = 1)]
        public string Work { get; set; } = string.Empty;

        [DataMember(Order = 2)]
        public bool Status { get; set; } = false;
    }
}
