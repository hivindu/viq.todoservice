﻿using System.Runtime.Serialization;

namespace VIQ.ToDos.Contract.Models
{
    [DataContract]
    public class DeleteToDoRequest
    {
        [DataMember(Order = 1)]
        public string ID { get; set; } = string.Empty;
    }
}
