﻿using System.Runtime.Serialization;
using VIQ.Service.Base.Models.Request;

namespace VIQ.ToDos.Contract.Models
{
    [DataContract]
    public class ListToDosRequest
    {
        public ListToDosRequest(GrpcPagedRequest paginationData)
        {
            this.PaginationData = paginationData;
        }

        public ListToDosRequest()
        {
            PaginationData = new GrpcPagedRequest();
        }

        [DataMember(Order = 1)]
        public GrpcPagedRequest PaginationData { get; set; }
    }
}
