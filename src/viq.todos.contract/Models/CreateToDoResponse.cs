﻿using System.Runtime.Serialization;
using System.Text.Json.Serialization;
using VIQ.Service.Base.Models;
using VIQ.Service.Base.Models.Responses;
using VIQ.ToDos.Contract.Models.Data;

#nullable enable
namespace VIQ.ToDos.Contract.Models
{
    [DataContract]
    public class CreateToDoResponse : GrpcResponse
    {
        public CreateToDoResponse(ToDoData todo)
        {
            ToDo = todo;
        }

        public CreateToDoResponse(GrpcError error)
        {
            Error = error;
        }

        public CreateToDoResponse()
        {
        }

        [JsonIgnore]
        [DataMember(Order = 1)]
        public override GrpcError? Error { get; set; }

        [DataMember(Order = 2)]
        public ToDoData? ToDo { get; set; }
    }
}
