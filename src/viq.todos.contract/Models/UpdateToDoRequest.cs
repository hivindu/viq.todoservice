﻿using System.Runtime.Serialization;

namespace VIQ.ToDos.Contract.Models
{
    [DataContract]
    public class UpdateToDoRequest
    {
        [DataMember(Order = 1)]
        public string Id { get; set; } = string.Empty;

        [DataMember(Order = 2)]
        public string Work { get; set; } = string.Empty;

        [DataMember(Order = 3)]
        public bool Status { get; set; } = false;
    }
}
