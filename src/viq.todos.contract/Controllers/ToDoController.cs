﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading;
using System.Threading.Tasks;
using VIQ.Service.Base.Controllers;
using VIQ.Service.Base.Models.Request;
using VIQ.Service.Base.Models.Responses;
using VIQ.Service.Base.Services.Context;
using VIQ.Service.Base.Services.Discovery;
using VIQ.ToDos.Contract.Models;
using VIQ.ToDos.Contract.Models.Data;
using VIQ.ToDos.Contract.Services;

namespace VIQ.ToDos.Contract.Controllers
{
    [Authorize]
    public class ToDoController : ApiController
    {
        private readonly IDiscoverableService<IToDoService> _toDoServiceDiscovery;
        private readonly IRequestData _requestData;

        public ToDoController(ILogger<ToDoController> logger, IRequestData requestData, IDiscoverableService<IToDoService> toDoServiceDiscovery)
            : base(logger)
        {
            _toDoServiceDiscovery = toDoServiceDiscovery;
            _requestData = requestData;
        }

        [HttpGet]
        [ProducesResponseType(typeof(PagedListResponse<ToDoData>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ServiceNotAvaliableResponse), StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> ListTodoAsync(
            [FromQuery] GrpcPagedRequest request,
            CancellationToken cancellationToken = default)
        {
            var toDoService = await _toDoServiceDiscovery.GetServiceAsync(cancellationToken).ConfigureAwait(false);

            if (toDoService is null)
            {
                return FormatResponse(new ServiceNotAvaliableResponse(nameof(IToDoService)));
            }

            var context = ConfigureCallContext(_requestData, cancellationToken: cancellationToken);

            var listTodoRequest = new ListToDosRequest(request);

            var response = await toDoService.ListToDoAsync(listTodoRequest, context).ConfigureAwait(false);

            return FormatResponse(response);
        }

        [HttpPost]
        [ProducesResponseType(typeof(ObjectResponse<CreateToDoResponse>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ServiceNotAvaliableResponse), StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> CreateToDoAsync(
            [FromBody] CreateToDoRequest request,
            CancellationToken cancellationToken = default)
        {
            var toDoService = await _toDoServiceDiscovery.GetServiceAsync(cancellationToken).ConfigureAwait(false);

            if (toDoService is null)
            {
                return FormatResponse(new ServiceNotAvaliableResponse(nameof(IToDoService)));
            }

            var context = ConfigureCallContext(_requestData, cancellationToken: cancellationToken);

            var response = await toDoService.CreateToDoAsync(request, context).ConfigureAwait(false);

            return FormatResponse(response);
        }

        [HttpPut]
        [ProducesResponseType(typeof(ObjectResponse<UpdateToDoResponse>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ServiceNotAvaliableResponse), StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> ListTodoAsync(
            [FromBody] UpdateToDoRequest request,
            CancellationToken cancellationToken = default)
        {
            var toDoService = await _toDoServiceDiscovery.GetServiceAsync(cancellationToken).ConfigureAwait(false);

            if (toDoService is null)
            {
                return FormatResponse(new ServiceNotAvaliableResponse(nameof(IToDoService)));
            }

            var context = ConfigureCallContext(_requestData, cancellationToken: cancellationToken);

            var response = await toDoService.UpdateToDoAsync(request, context).ConfigureAwait(false);

            return FormatResponse(response);
        }

        [HttpDelete]
        [ProducesResponseType(typeof(ObjectResponse<DeleteToDoResponse>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ServiceNotAvaliableResponse), StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> RemoveToDoAsync(
            [FromBody] DeleteToDoRequest request,
            CancellationToken cancellationToken = default)
        {
            var toDoService = await _toDoServiceDiscovery.GetServiceAsync(cancellationToken).ConfigureAwait(false);

            if (toDoService is null)
            {
                return FormatResponse(new ServiceNotAvaliableResponse(nameof(IToDoService)));
            }

            var context = ConfigureCallContext(_requestData, cancellationToken: cancellationToken);

            var response = await toDoService.RemoveToDoAsync(request, context).ConfigureAwait(false);

            return FormatResponse(response);
        }
    }
}
